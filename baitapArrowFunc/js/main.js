const xuatDanhSachMau = () => {
  const colorList = [
    "#B888F8",
    "#7FF71F",
    "#909078",
    "#2840F8",
    "#F89800",
    "#C820F8",
    "#28F828",
    "#F8F800",
    "#F87878",
    "#F84040",
  ];

  colorList.forEach(function (item, index) {
    var content = `<div class="color-item">
    <div id="select-icon-${index}" class="button-color-icon select-icon-hide ${
      index === 0 ? "select-icon-show" : ""
    }">></div>
    <div class="button-color" style="background-color:${item}" onclick="thayDoiMau('${item}', ${index})"></div>
    </div>`;

    document
      .getElementById("colorContainer")
      .insertAdjacentHTML("beforeend", content);
  });
};

xuatDanhSachMau();

function thayDoiMau(color, index) {
  document.documentElement.style.setProperty("--primary-color", color);

  document
    .getElementsByClassName("select-icon-show")[0]
    .classList.remove("select-icon-show");

  document
    .getElementById(`select-icon-${index}`)
    .classList.add("select-icon-show");
}
